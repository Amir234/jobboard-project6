<?php

namespace App\Entity;

use App\Repository\ApplicantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ApplicantRepository::class)]
class Applicant
{
    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    private ?Uuid $id = null;

    #[Assert\NotBlank]
    #[Assert\Length(min: 5, max: 50)]
    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[Assert\NotBlank]
    #[Assert\Length(min: 5, max: 50)]
    #[ORM\Column(type: Types::TEXT)]
    private ?string $contactInformation = null;

    #[Assert\NotBlank]
    #[Assert\Length(min: 5, max: 50)]
    #[ORM\Column(length: 255)]
    private ?string $jobPreferences = null;

    #[ORM\ManyToMany(targetEntity: Jobboard::class, inversedBy: 'applicants')]
    private Collection $jobsApplied;

    public function __construct()
    {
        $this->jobsApplied = new ArrayCollection();
    }

   

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getContactInformation(): ?string
    {
        return $this->contactInformation;
    }

    public function setContactInformation(string $contactInformation): static
    {
        $this->contactInformation = $contactInformation;

        return $this;
    }

    public function getJobPreferences(): ?string
    {
        return $this->jobPreferences;
    }

    public function setJobPreferences(string $jobPreferences): static
    {
        $this->jobPreferences = $jobPreferences;

        return $this;
    }

    /**
     * @return Collection<int, Jobboard>
     */
    public function getJobsApplied(): Collection
    {
        return $this->jobsApplied;
    }

    public function addJobsApplied(Jobboard $jobsApplied): static
    {
        if (!$this->jobsApplied->contains($jobsApplied)) {
            $this->jobsApplied->add($jobsApplied);
        }

        return $this;
    }

    public function removeJobsApplied(Jobboard $jobsApplied): static
    {
        $this->jobsApplied->removeElement($jobsApplied);

        return $this;
    }
    public function toArray(): array
    {
        return [
                'id' => (string)$this->getId(),
                'name' => $this->getName(),
                'description' => $this->getContactInformation(),
                'contactInformation' => $this->getJobPreferences(),
                'jobboard' => $this->jobsApplied
        ];
    }


}
