<?php

namespace App\Controller;

use App\Entity\Jobboard;
use App\Repository\CompanyRepository;
use App\Repository\JobboardRepository;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Attributes as OA;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route(path: '/api/v1')]
#[OA\Tag(name: 'jobboard')]
class JobboardController extends AbstractController
{
    use JsonResponseFormat;
    
    #[Route(path: "/jobboards", methods:["POST"])]
    #[OA\Post(description: "Create jobboard." )]
    #[OA\Tag(name: 'jobboard')]
    #[OA\Parameter(
        name: 'jobboard',
        description: 'The name of the jobboard',
        in: 'query',
        schema: new OA\Schema(type: 'string')
    )]
    #[OA\Parameter(
        name: 'description',
        description: 'The description of the jobboard',
        in: 'query',
        schema: new OA\Schema(type: 'string')
    )]
    #[OA\RequestBody(
        description: "json to create jobboard"
    )]
    #[OA\Response(
        response: 201,
        description: "returns the ID of the jobboard"
    )]
    public function create(JobboardRepository $jobboardRepository, CompanyRepository $companyRepository, Request $request, ValidatorInterface $validator): Response
    {
        $jsonParams = json_decode($request->getContent(), true);

        $jobboard = new Jobboard();
        $jobboard->setTitle($jsonParams['title']);
        $jobboard->setDescription($jsonParams['description']);
        $jobboard->setRequiredSkills($jsonParams['requiredSkills']);
        $jobboard->setExperience($jsonParams['experience']);

        $company = $companyRepository->find($jsonParams['company']);
        $jobboard->setCompany($company);

        $violations = $validator->validate($jobboard);

        if (count($violations)) {
            return $this->JsonResponse('Invalid inputs', $this->getViolationsFromList($violations), 400); 
        }

        $jobboardRepository->save($jobboard, true);
        return $this->JsonResponse('Jobboard created', $jobboard->toAarray(), 201); 
    }

    #[Route(path:"/jobboards/{id}", methods: ["GET"])]
    public function findById(JobboardRepository $jobboardRepository, string $id): Response
    {
        $jobboard = $jobboardRepository->find($id);
        if ($jobboard == null) {
            return $this->JsonResponse('jobs not found', ['id' => $id], 404);
        }
        return $this->JsonResponse('jobs found', $jobboard->toAarray());
    }

    #[Route(path: "/jobboards", methods: ["GET"])]
    #[OA\Get(description: "Return all the jobs with optional filters")]
    #[OA\QueryParameter(name: "title", example: "admin officer")]
    #[OA\QueryParameter(name: "companyName", example: "adidad")]
    #[OA\QueryParameter(name: "companyLocation", example: "Lekki")]
    #[OA\Response(
        response: 200,
        description: "Lists of jobs response",
        content: new OA\JsonContent(ref: new Model(type: ResponseDto::class))
    )]
    public function findAll(EntityManagerInterface $entityManager, Request $request): Response
    {
        $title = $request->get('title');
        $companyName = $request->get('name');
        $companyLocation = $request->get('location');

        $queryBuilder = $entityManager
        ->getRepository(Jobboard::class)
        ->createQueryBuilder('j');
     

        if ($title !== null) {
            $queryBuilder->andWhere('j.title LIKE :title')
                ->setParameter(':title', '%$title%');
        }
        if ($companyName !== null) {
            $queryBuilder->andWhere('c.name LIKE :name')
                ->setParameter(':name', "%$companyName%");
        }

        $queryBuilder->orderBy('j.title', 'ASC');

        $jobboards = $queryBuilder->getQuery()->execute();

        $jobboardArray = [];
        /** @var Jobboard $jobboard */
        foreach ($jobboards as $jobboard) {
            $jobboardArray[] = $jobboard->toAarray();
        }
        return $this->jsonResponse('List of Jobs', $jobboardArray);
    }
    #[Route(path: "/jobboards/{id}", methods: ["PUT"])]
    public function update(JobboardRepository $jobboardRepository, Request $request, string $id): Response
    {
        $jsonparams = json_decode($request->getContent(), true);

        $jobboard = $jobboardRepository->find($id);
        if ($jobboard == null) {
            return $this->jsonResponse("Job not found", ['id' => $id], 404);
        }
        $jobboard->setTitle($jsonparams['title']);
        $jobboard->setDescription($jsonparams['description']);

        $jobboardRepository->save($jobboard, true);

        $data = ['id' => (string)$jobboard->getId()];
        return $this->JsonResponse('Jobboard updated.', $data, 200);
    }

    #[Route(path: "/jobboards/{id}", methods: ["DELETE"])]
    public function delete(JobboardRepository $jobboardRepository, Request $request, string $id): Response
    {
        $jobboard = $jobboardRepository->find($id);
        if ($jobboard == null) {
            return $this->jsonResponse("Job not found", ['id' => $id], 404);
        }

        $jobboardRepository->remove($jobboard, true);

        return $this->JsonResponse('Job removed successfully', []);
    }  
}