<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\ConstraintViolationListInterface;

trait JsonResponseFormat
{
    private function getViolationsFromList($violations): array
    {
        $errorData = [];
       
        /**@var ConstraintViolationInterface $violations */
        foreach ($violations as $violation) {
            $errorData[$violation->getPropertyPath()][] = $violation->getMessage();         
        }
        return $errorData;
    }

    private function JsonResponse(string $message, array $data, int $statusCode = 200): JsonResponse
    {
        return new JsonResponse((array)new ResponseDto($message, $data, $statusCode));
    }

}