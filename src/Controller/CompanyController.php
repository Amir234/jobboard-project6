<?php

namespace App\Controller;

use App\Entity\Company;
use App\Entity\User;
use App\Repository\CompanyRepository;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Attributes as OA;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route(path: '/api/v1')]
#[OA\Tag(name: 'comapny')]
class CompanyController extends AbstractController
{
    use JsonResponseFormat;

    #[Route(path: "/companies", methods:["POST"])]
    #[OA\Post(description: "Create company" )]
   
    
    #[OA\RequestBody(
        description: "json to create company",
        content: new OA\JsonContent( 
            properties: [
                new OA\Property(property: "name", type: "string", example: "CompanyName"),
                new OA\Property(property: "description", type: "string", example: "Description of the Company"),
                new OA\Property(property: "contactInformation", type: "string", example: "Company contact information"),
                new OA\Property(property: "location", type: "string", example: "CompanyLocation")
            ]
        )
    )]
    #[OA\Response(
        response: 201,
        description: "returns the ID of the company",
        content: new OA\JsonContent( 
            properties: [
                new OA\Property(property: "statusCode", type: "integer", example: 201),
                new OA\Property(property: "message", type: "string", example: "Company created"),
                new OA\Property(property: "data", type: "object"),
            ],
        )
    )]
    #[OA\Response(
        response: 400,
        description: "Invalid argument",
        content: new OA\JsonContent(ref: new Model(type: Response::class))
    )]
     
    public function create(CompanyRepository $companyRepository, Request $request, ValidatorInterface $validator): Response
    {
       $jsonParams = json_decode($request->getContent(), true);


       $company = new Company();
       $company->setName($jsonParams['name']);
       $company->setDescription($jsonParams['description']);
       $company->setContactInformation($jsonParams['contactInformation']);
       $company->setLocation($jsonParams['location']);

       $violations = $validator->validate($company);

       if (count($violations)) {
           $errorData = $this->getViolationsFromList($violations);
           return $this->JsonResponse('Invalid input', $errorData, 400);
       }

       $companyRepository->save($company, true);
       
       $data = ['id' => (string)$company->getId()];
       return $this->JsonResponse('Company created.', $data, 201);
    }

    #[Route(path:"/companies", methods: ["GET"])]
    #[OA\Get(description: "Find all companies")]
    public function findAll(CompanyRepository $companyRepository, TokenStorageInterface $storage): Response
    {
        $token = $storage->getToken();
        $user = $token?->getUser();
        if (! $user instanceof User) {
            throw new \RuntimeException('Invalid user from token');
        }
        $companies = $companyRepository->findAll();

        $response = [];
        foreach ($companies as $company) {
            $response[] = $company->toArray();
        }
        return $this->JsonResponse('List of companies', $response);
    }
    
    #[Route(path:"/companies/{id}", methods: ["GET"])]
    #[OA\Get(description: "Find company by Id")]
    public function findById(CompanyRepository $companyRepository, string $id): Response
    {
        $company = $companyRepository->find($id);
        if ($company == null) {
            return $this->JsonResponse('Company not found', ['id' => $id], 404);
        }

        return $this->JsonResponse('Company by ID', $company->toArray());

    }
    #[Route(path:"/companies/{id}", methods: ["PUT"])]
    #[OA\Get(description: "Update company")]
    public function update(CompanyRepository $companyRepository, Request $request, string $id): Response
    {
        $company = $companyRepository->find($id);
        if ($company == null) {
            return $this->JsonResponse('Company not found', ['id' => $id], 404);
        }

        $jsonParams = json_decode($request->getContent(), true);

        $company->setName($jsonParams['name']);
        $company->setDescription($jsonParams['description']);

        $companyRepository->save($company, true);

        return $this->JsonResponse('Company updated', $company->toArray());

    }
    #[Route(path:"/companies/{id}", methods: ["DELETE"])]
    #[OA\Get(description: "Delete company")]
    public function delete(CompanyRepository $companyRepository, string $id): Response
    {
        $company = $companyRepository->find($id);
        if ($company == null) {
            return $this->JsonResponse('Company not found', ['id' => $id], 404);
        }

        $companyRepository->remove($company, true);
        return $this->JsonResponse('Company removed', $company->toArray());
    }
}