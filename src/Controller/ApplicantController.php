<?php

namespace App\Controller;

use App\Entity\Applicant;
use App\Entity\User;
use App\Repository\ApplicantRepository;
use App\Repository\JobboardRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Attributes as OA;
use RuntimeException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Nelmio\ApiDocBundle\Annotation\Model;

#[Route(path: '/api/v1')]
#[OA\Tag(name: 'applicant')]
class ApplicantController extends AbstractController
{
    use JsonResponseFormat;

    #[Route(path: "/applicant", methods: ['POST'])]
    #[OA\Post(description: "Create applicant" )]
   
    
    #[OA\RequestBody(
        description: "json to create applicant",
        content: new OA\JsonContent( 
            properties: [
                new OA\Property(property: "name", type: "string", example: "ApplicantName"),
                new OA\Property(property: "contactInformation", type: "string", example: "Applicant contact information"),
                new OA\Property(property: "jobPreferences", type: "string", example: "Job Prefences"),
                new OA\Property(property: "jobApplied", type: "string", example: "Applicant jobs Applied")
            ]
        )
    )]
    #[OA\Response(
        response: 201,
        description: "returns the ID of the company",
        content: new OA\JsonContent( 
            properties: [
                new OA\Property(property: "statusCode", type: "integer", example: 201),
                new OA\Property(property: "message", type: "string", example: "Applicant created"),
                new OA\Property(property: "data", type: "object"),
            ],
        )
    )]
    #[OA\Response(
        response: 400,
        description: "Invalid argument",
        content: new OA\JsonContent(ref: new Model(type: Response::class))
    )]
    public function create(ApplicantRepository $applicantRepository, JobboardRepository $jobboardRepository, Request $request, ValidatorInterface $validator): Response
    {
    
       $jsonParams = json_decode($request->getContent(), true);

       $applicant = new Applicant();
       $applicant->setName($jsonParams['name']);
       $applicant->setContactInformation($jsonParams['contactInformation']);
       $applicant->setJobPreferences($jsonParams['jobPreferences']);
       $jobboard = $jobboardRepository->find($jsonParams['jobboardApplied']);
       $applicant->addJobsApplied($jobboard);

       $violations = $validator->validate($applicant);

       if (count($violations)) {
        $errorData = $this->getViolationsFromList($violations);
        return $this->jsonResponse('Invalid input', $errorData, 400); 
       }
       $applicantRepository->save($applicant, true);
       $data = ['id' => (string)$applicant->getId()];
       return $this->JsonResponse('Applicant created.', $data, 201);    
    } 

    #[Route(path: "/applicant/{id}", methods: ["GET"])]
    #[OA\Get(description: "Return an applicant by its ID")]
    public function findById(ApplicantRepository $applicant, string $id): Response
    {
        $applicant = $applicant->find($id);
        if ($applicant === null) {
            return $this->jsonResponse('Applicant not found', ['id' => $id], 404);
        }
        return $this->JsonResponse('Applicant by ID', $applicant->toArray());
    }

    #[Route(path: "/applicant", methods: ['GET'])]
    #[OA\Get(description: "Return all applicants")]
    public function findAll(ApplicantRepository $applicant, TokenStorageInterface $tokenInterface): Response
    {
        $token = $tokenInterface->getToken();
        $user = $token?->getUser();

        if (!$user instanceof User) {
            throw new RuntimeException('Invalid user from token');
        }
        $applicants = $applicant->findAll();

        $response = [];
        foreach ($applicants as $applicant) {
            $response[] = $applicant->toArray();
        }
        return $this->JsonResponse('List of applicant', $response);
    }

    #[Route(path: "/applicants/{id}", methods: ["PUT"])]
    #[OA\Get(description: "Update applicant")]
    public function update(ApplicantRepository $applicantRepository, Request $request, ValidatorInterface $validator, string $id): Response
    {
        $jsonParams = json_decode($request->getContent(), true);

        $applicant = $applicantRepository->find($id);
        if ($applicant == null) {
            return $this->jsonResponse("Applicant not found", ['id' => $id], 404);
        }

        $jsonParams = json_decode($request->getContent(), true);

        $applicant->setName($jsonParams['name']);
        $applicant->setContactInformation($jsonParams['contactInformation']);
        $applicant->setJobPreferences($jsonParams['jobPreferencec']);

        $applicantRepository->save($applicant, true);

        return $this->JsonResponse('Applicant updated', $applicant->toArray());   
    }
    #[Route(path:"/applicants/{id}", methods: ["DELETE"])]
    #[OA\Get(description: "Delete applicant")]
    public function delete(ApplicantRepository $applicantRepository, string $id): Response
    {
        $applicant = $applicantRepository->find($id);
        if ($applicant == null) {
            return $this->JsonResponse('Applicant not found', ['id' => $id], 404);
        }

        $applicantRepository->remove($applicant, true);
        return $this->JsonResponse('Company removed', $applicant->toArray());

    }
}
