<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230725114207 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE applicant_jobboard (applicant_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', jobboard_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_46A8220097139001 (applicant_id), INDEX IDX_46A8220094B0163 (jobboard_id), PRIMARY KEY(applicant_id, jobboard_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE applicant_jobboard ADD CONSTRAINT FK_46A8220097139001 FOREIGN KEY (applicant_id) REFERENCES applicant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE applicant_jobboard ADD CONSTRAINT FK_46A8220094B0163 FOREIGN KEY (jobboard_id) REFERENCES jobboard (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE applicant_jobboard DROP FOREIGN KEY FK_46A8220097139001');
        $this->addSql('ALTER TABLE applicant_jobboard DROP FOREIGN KEY FK_46A8220094B0163');
        $this->addSql('DROP TABLE applicant_jobboard');
    }
}
