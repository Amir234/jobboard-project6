# JOB BOARD API

- This job Board API is a RESTful web service built using Symfony, this allows client to manage jo postings and apply for a job on the job-board.

# Requirements

- PHP 8.1
- Composer
- MAMP

# Install 

 1. Clone the reposository:

   git clone https://gitlab.com/Amir234/jobboard-project6

  2. Access the directory:

   cd jobboard-api/

  3. Install the Composer dependencies:

    composer install

  4. Go to MySQL and create the database job-board

  5. Create a file .env.local and add your database connection. Example:

  DATABASE_URL="mysql://root:@localhost:3306/job-board"

  6. Create the tables:

  php bin/console doctrine:migrations:migrate

  7. Run the application:

  symfony server:start
  # or
  php -S localhost:8000 -t public  

  8. Go to http://localhost:8000 

# Routes

- To acces the API documentation: http://localhost:8000/api/doc

# Authorize default
- GET: /api/doc.json
# auth
- /POST: /api/login_check
- POST: /api/v1/register

# applicant
- GET: /api/v1/applicant
- POST: /api/v1/applicant
- GET: /api/v1/applicant/{id}
- PUT: /api/v1/applicants/{id}
- DELETE: /api/v1/applicants/{id}

# comapny
- GET: /api/v1/companies
- POST: /api/v1/companies
- GET: /api/v1/companies/{id}
- PUT: /api/v1/companies/{id}
- DELETE: /api/v1/companies/{id}

# jobboard
- GET: /api/v1/jobboards
- POST: /api/v1/jobboards
- GET: /api/v1/jobboards/{id}
- PUT: /api/v1/jobboards/{id}
- DELETE: /api/v1/jobboards/{id}

# Quality Tools

- PHP CS Fixer to check the code style and PHPStan for static analysis is required.

# Install PHP CodeSniffer

- composer require friendsofphp/php-cs-fixer

- Run PHP CodeSnifer: ./vendor/bin/phpcs --standard=PSR12 src/

- Run PHP CodeSniffer Fixer: ./vendor/bin/phpcbf --standard=PSR12 src/

# Install PHPstan

- composer require --dev phpstan/phpstan-symfony

- Run PHPstan: php vendor/bin/phpstan analyze

# Install PHPUnit

- composer require --dev symfony/phpunit-bridge

- Run Unit Tests: php bin/phpunit
